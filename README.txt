=============================================================================
TDWeb - (the start of) a WebApp interface to TradeDangerous.


Requires:
  >= Python 3.4
  bottle.py (tested with 0.12)
      http://bottlepy.org/docs/dev/index.html
  TradeDangerous
      https://bitbucket.org/kfsone/tradedangerous/overview

Setup:
  Install bottle.py OR download it directly into the folder with TDWeb
  Copy ./dist/tdweb.conf-dist to ./tdweb.conf
  Edit the values in ./tdweb.conf to match your needs.
  Run ./tdweb.py


  Only basic functionality's implemented so far. The UI end is minimal
to nonexistent. The backend needs the rest of the commands implemented.
Right now, it takes input for the commands implemented and returns
raw json for them. Eventually(TM) that will be parsed by a client
side javascript that will then render it into the page.

  This currently runs on the wsgiref backend. It's neither designed
for use as a shared service nor will it currently perform well in that
use-case. On the other side of that same coin, bottle.py makes it easy
to switch that out with a higher performance backend. See:

http://bottlepy.org/docs/dev/deployment.html

  There is not a proper API definition. Input fields are case sensitive,
they also change names somewhat randomly from one command to the next.

  The reason bottle.py was chosen over flask was, simply, becasue it's
small, easy to embed, and still easy to work with.

-----

  All the real credit goes to Oliver "kfsone" Smith and the others who
make TradeDangerous itself happen. 

=============================================================================

Current state of 'how it works':

It doesn't. :) (though it puts on a great act...)

A list of implemented commands is generated to do two things:
 - generate the nav bar (in views/base.tpl)
 - set the routes for those commands.
That list comes from WebCommandIndex() (in web_commands/__init__.py), and,
unsurprisingly, vaguely mirrors TD's commands/CommandIndex() setup.

Each module in web_commands/ implements setup_routes() and the callbacks it
references. They also implement a render() method , which is used translate
the output of the command into json to return to the client. Their POST
handler (generally just named post()) builds a set of arguments mirroring
their trade.py command line equivalent, then passes that list off to
tdweb_run.command() along with a reference to their render() method.
tdweb_run.command() passes the arguments to TD's CommandIndex() parse()
method, then runs the command it returns, and finally passes the results to
the render() method.

tdweb.py only defines the /, /ui and the static file handlers. The static
file handlers are picky. PNGs in the /images/ route, .js in the /js/
route, and .css in the /css/ route. All of these are served from their
respective folder under static/. Also, /favicon.ico is served from under
static/images/.

User-side errors are handed back to the user in an 'error' field in the
json response.

-----

BUY, LOCAL, NAV, RARES, and SELL exist and appear to work (I can't guarantee I
don't have a typo'd form field somewhere)

OLDDATA will be fairly trivial, STATION and SHIPVENDOR as well.

RUN's the next thing I'll likely address. It takes the render() stage a
few steps further off the deep end, being the most in-depth output set
TD has to offer.

IMPORT and EXPORT will require a little more effort, and UPDATE's going
to require actually planning before diving into code.

-----

Templates (under views/) are minimal, a base wrapper, a form generator, and
a blank placeholder for /ui/.

-----

Documentation ... yeah, this is it. There's not a lot in the way of comments
at present either.

-----

