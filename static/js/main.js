/*global $,Backbone,_,LRUCache,QueryParser,PNotify,console*/
/* Above line exists 'cause JSLint's a PITA */

// Wrap everything in its own function.
(function () {
  'use strict';

  // Bottle.py goes nuts when it finds a '%' symbol. Allow
  // both <% %> and <@ @>.
  _.templateSettings = {
    evaluate: /<[%@]([\s\S]+?)[%@]>/g,
    interpolate: /<[%@]=([\s\S]+?)[%@]>/g,
    escape: /<[%@]-([\s\S]+?)[%@]>/g
  };

  /*---------------------------------------------------------------------------
   * Backbone.js setup and testing.
   *-------------------------------------------------------------------------*/
  // Start by grabbing a clean namespace to keep things organized.
  var TDWeb;

  TDWeb = {};
  TDWeb.Models = {};
  TDWeb.Data = {};
  TDWeb.Config = {};
  TDWeb.Views = {};
  TDWeb.Controllers = {};
  window.TDWeb = TDWeb;

  TDWeb.commandList = [
    'run', 'buy', 'sell', 'rares', 'nav', 'local',
    'trade'/*, 'station', 'system', 'data'*/
  ];

  TDWeb.Config.optionsMap = {
    avoid: {type: 'list', friendly: 'Avoid List (comma separated)'},
    blackMarket: {type: 'bool', friendly: 'Require Black Market'},
    capacity: {type: 'int', friendly: 'Cargo Capacity (T)'},
    credits: {type: 'int', friendly: 'Starting Credits'},
    emptyLy: {type: 'float', friendly: 'Jump Range (empty)'},
    endJumps: {type: 'int', friendly: 'End Jumps (empty)'},
    from: {type: 'place', friendly: 'Origin (station/system)'},
    fromStn: {type: 'station', friendly: 'Origin (station)'},
    gtCr: {type: 'int', friendly: 'Greater than (cr)'},
    hops: {type: 'int', friendly: 'Hop count'},
    insurance: {type: 'int', friendly: 'Insurance'},
    item: {type: 'item', friendly: 'Item'},
    itemLimit: {type: 'int', friendly: 'Cargo limit per item (T)'},
    jumps: {type: 'int', friendly: 'Jumps per Hop'},
    ladenLy: {type: 'float', friendly: 'Jump Range (laden)'},
    lsMax: {type: 'int', friendly: 'Max ls-from-star'},
    lsPenalty: {type: 'float', friendly: 'ls-from-star penalty'},
    ltCr: {type: 'int', friendly: 'Less than (cr)'},
    lySearchRadius: {type: 'float', friendly: 'Search Radius (lys)'},
    maxAge: {type: 'float', friendly: 'Maximum Data Age (days)'},
    maxRoutes: {type: 'int', friendly: '* Maximum Routes'},
    near: {type: 'system', friendly: 'Near System'},
    padSize: {type: 'padsize', friendly: 'Pad Size(s)'},
    profitMargin: {type: 'float', friendly: 'Profit Margin/Hop'},
    pruneHops: {type: 'int', friendly: '* Prune Hops'},
    pruneScore: {type: 'float', friendly: '* Prune Score'},
    quantity: {type: 'int', friendly: 'Minimum Supply Quantity'},
    refuelJumps: {type: 'int', friendly: 'Max jumps between refuelling'},
    resultLimit: {type: 'int', friendly: 'Limit result count'},
    routes: {type: 'int', friendly: 'Number of Routes to return'},
    sort: {type: 'sort', friendly: 'Sort By'},
    startJumps: {type: 'int', friendly: 'Start Jumps (empty)'},
    to: {type: 'place', friendly: 'Destination (station/system)'},
    toStn: {type: 'station', friendly: 'Destination (station)'},
    towards: {type: 'place', friendly: 'Route towards (station/system)'},
    unique: {type: 'bool', friendly: 'Unique Station Each Hop'},
    useEmpty: {type: 'bool', friendly: 'Use Empty Jump Range'},
    via: {type: 'list', friendly: 'Via List'},
  };

  TDWeb.Config.getOptions = function(cmd) {
    var cmd_allowed;
    cmd_allowed = [ ];

    switch (cmd) {
      case 'run':
        cmd_allowed = [
          'credits', 'hops', 'jumps', 'startJumps', 'endJumps', 'insurance',
          'blackMarket', 'maxAge', 'maxRoutes', 'pruneScore', 'pruneHops',
          'routes', 'lsMax', 'unique', 'capacity', 'lsPenalty', 'itemLimit',
          'profitMargin', 'padSize', 'towards'
        ];
        /* falls through */
      case 'nav':
        if (cmd == 'nav') {
          cmd_allowed = [ 'useEmpty', 'refuelJumps' ];
        }
        return _.flatten([ [
          'from', 'to', 'ladenLy', 'emptyLy', 'avoid', 'via'
        ], cmd_allowed ]);

      case 'buy':
        cmd_allowed = ['quantity'];
        /* falls through */
      case 'sell':
        return _.flatten([ [
          'item', 'near', 'sort', 'padSize', 'lySearchRadius', 'resultLimit',
          'ltCr', 'gtCr'
        ], cmd_allowed ]);

      case 'rares':
        cmd_allowed = ['sort', 'resultLimit'];
        /* falls through */
      case 'local':
        return _.flatten([ [
          'near', 'padSize', 'lySearchRadius'
        ], cmd_allowed ]);

      case 'trade':
        return [ 'fromStn', 'toStn' ];

      //case 'system':
      //case 'station':
      //case 'data':
      default:
        return [ ];
    }
  };

  TDWeb.Controllers.handleCommand = function(cmd, opts) {
    var allowed, out;

    allowed = TDWeb.Config.getOptions(cmd);
    TDWeb.Config.common.set(_.pick(opts, allowed));

    TDWeb.Data.results.clear({silent: true});
    TDWeb.Data.results.set({commandName: cmd});

    out = $.param(TDWeb.Config.common.pick(allowed));
    TDWeb.router.navigate('/' + cmd + '?' + out, false);

    $.get('/api/' + cmd + '?' + out)
      .done(function(data, textStatus, jqXHR) {
        // Success
        try {
          out = JSON.parse(data);
        } catch (e) {
          out = "Failed to parse response from command: " + cmd;
          Backbone.trigger('notify:error', out);
          return;
        }
        TDWeb.Data.results.set(out);
      }).fail(function(jqXHR, textStatus, errorThrown) {
        // Fail
        try {
          out = JSON.parse(jqXHR.responseText).error;
        } catch(e) {
          out = 'Error running command: ' + cmd;
        }
        Backbone.trigger('notify:error', out);
      }
    );
  };

  TDWeb.Data.getPlace = function(type, name, callback) {
    var tmp, cache, url, model;
    switch (type.toLowerCase()) {
      case 'station':
        cache = TDWeb.Data.StationCache;
        model = TDWeb.Models.Station;
        url = '/api/stations/' + encodeURI(name)
        break;
      case 'system':
        name = name.toUpperCase();
        cache = TDWeb.Data.SystemCache;
        model = TDWeb.Models.System;
        url = '/api/systems/' + encodeURI(name)
        break;
      default:
        cache = false;
        model = false;
        url = '/api/places/' + encodeURI(name)
    }

    if (cache && cache.find(name) !== undefined) {
      callback(cache.get(name));
    } else {
      $.get(url)
        .done(function(data, textStatus, jqXHR) {
          // Success
          try {
            tmp = JSON.parse(data);
            name = tmp.id;
          } catch (e) {
            tmp = "Failed to parse response for station: " + name;
            Backbone.trigger('notify:error', tmp);
            callback(undefined);
          }
          if (!cache) {
            if (typeof tmp.system !== 'undefined') {
              // Stations have a 'system' element.
              type = 'station';
              cache = TDWeb.Data.StationCache;
              model = TDWeb.Models.Station;
            } else {
              type = 'system';
              cache = TDWeb.Data.SystemCache;
              model = TDWeb.Models.System;
            }
          }
          if (cache.find(name) === undefined) {
            tmp = new model(tmp);
            cache.put(tmp.get('id'), tmp);
            callback(tmp);
          } else {
            callback(cache.get(name));
          }
        }).fail(function(jqXHR, textStatus, errorThrown) {
          // Fail
          try {
            tmp = JSON.parse(jqXHR.responseText).error;
          } catch(e) {
            tmp = 'Error getting '+type+': ' + name;
          }
          Backbone.trigger('notify:error', tmp);
          callback(undefined);
        }
      );
    }
  };

  TDWeb.Models.BaseModel = Backbone.Model.extend();
  TDWeb.Views.BaseView = Backbone.View.extend();

  // [ "run", "buy", "sell", "rares", "nav", "local",
  //   "trade", "station", "system", "data" ]
  // Set up routing.
  TDWeb.Router = Backbone.Router.extend({
    routes: {
      '': 'home',
      'run': 'run',
      'buy': 'buy',
      'sell': 'sell',
      'rares': 'rares',
      'nav': 'nav',
      'local': 'local',
      'trade': 'trade',
      'station': 'station',
      'system': 'system',
      'data': 'data'
    },

    titles: {
      'base': 'TDWeb',
      'home': 'TDWeb'
    },

    initialize: function(options){
      var that = this;

      this.on('route', function(route, args) {

        if(that.titles) {
          if(that.titles[route]) {
            document.title = that.titles[route];
          } else {
            document.title = that.titles.base + ' - ' + route.toTitleCase();
          }
        }

        TDWeb.Config.common.set({commandName: route.toLowerCase()});

        if (args !== null && typeof(args) === 'object' && args["0"] !== null) {
          TDWeb.Controllers.handleCommand(route, QueryParser.parse(args));
        }

      });
    }
  });

  TDWeb.router = new TDWeb.Router();

  TDWeb.debug = false;
  TDWeb.debug_log = function (msg) {
    if (TDWeb.debug) {
      console.log('TDWeb: ', msg);
    }
  };

  TDWeb.init_routing = function () {
    Backbone.trigger('notify:debug', 'Enabling Backbone Routing');
    Backbone.history.start({
      pushState: true
    });

    // Enable pushState.
    if (Backbone.history && Backbone.history._hasPushState) {
      var $document = $(window.document);
      var openLinkInTab = false;

      $document.keydown(function (e) {
        if (e.ctrlKey || e.keyCode === 91) {
          openLinkInTab = true;
        }
      });

      $document.keyup(function () {
        openLinkInTab = false;
      });

      $document.on('click', 'a', function (e) {
        var $this = $(this);
        var href = $this.attr('href');
        var domain = $this.prop('hostname');

        // if the href is not equal to outpost.travel
        var isCSRF = domain !== window.document.location.hostname;
        var hasHashLink = href.indexOf('#') === -1;

        // if it's been indicated that we don't want to open in a new tab
        // and that the link is the same domain then preventDefault
        // protection againts internal #div links
        if (!openLinkInTab && !isCSRF && hasHashLink) {
          e.preventDefault();
          Backbone.history.navigate(href, true);
        }
      });
    }
  };

  TDWeb.start = function () {
    ///////////////////////
    // Models
    ///////////////////////
    TDWeb.Models.System = TDWeb.Models.BaseModel.extend({
      url: function () {
        return '/api/systems/' + this.id;
      }
    });

    TDWeb.Models.Station = TDWeb.Models.BaseModel.extend({
      url: function () {
        return '/api/stations/' + this.id;
      }
    });

    TDWeb.Models.Item = TDWeb.Models.BaseModel.extend({
      url: function () {
        return '/api/items/' + this.id;
      }
    });

    TDWeb.Models.Ship = TDWeb.Models.BaseModel.extend({
      url: function () {
        return '/api/ships/' + this.id;
      }
    });

    TDWeb.Config.ConfigModel = TDWeb.Models.BaseModel.extend();
    TDWeb.Data.ResultsModel = TDWeb.Models.BaseModel.extend();

    ///////////////////////
    // Collections/Caches
    ///////////////////////
    TDWeb.Data.SystemCache = new LRUCache(20);
    TDWeb.Data.StationCache = new LRUCache(20);

    TDWeb.Data.ItemCollection = Backbone.Collection.extend({
      model: TDWeb.Models.Item,
      url: "/api/items",
      categories: function () {
        return _.uniq(this.pluck('category'));
      }
      });

    TDWeb.Data.ShipCollection = Backbone.Collection.extend({
      model: TDWeb.Models.Ship,
      url: "/api/ships"
    });

    ///////////////////////
    // Populate base data
    ///////////////////////

    TDWeb.Data.results = new TDWeb.Data.ResultsModel();

    TDWeb.Data.items = new TDWeb.Data.ItemCollection();
    //TDWeb.Data.items.fetch();

    TDWeb.Data.ships = new TDWeb.Data.ItemCollection();
    //TDWeb.Data.ships.fetch();

    TDWeb.Config.common = new TDWeb.Config.ConfigModel();


    ///////////////////////
    // Views
    ///////////////////////
    TDWeb.Views.Content = TDWeb.Views.BaseView.extend({
      // Statically assigned an existing element. Doesn't
      // need injected into the DOM tree after rendering.
      el: '#content',
      template: _.template($('#template-base').html()),

      initialize: function () {
        var $w = $(window);
        var $mb = $('#menubar');
        var $el = this.$el;

        this.render().$el.height(function () {
          return ($w.height() - $mb.height()) * 0.9;
        });

        $w.bind("resize", _.debounce(function () {
          $el.height(function () {
            return ($w.height() - $mb.height()) * 0.9;
          });
        }, 200));
      },

      render: function () {
        this.$el.html(this.template());
        return this;
      }
    });

    TDWeb.Views.ConfigOverview = TDWeb.Views.BaseView.extend({
      tagName: 'table',
      className: 'config-overview',
      
      events: {
        'click .edit-button': 'editButton',
        'click .run-button': 'runButton'
      },

      editButton: function() {
        TDWeb.Views.confEditor.$el.dialog("open");
        return;
      },
      
      runButton: function() {
        var cmd = this.model.get('commandName');
        if (cmd !== undefined && cmd !== 'home') {
          TDWeb.Controllers.handleCommand(
            cmd, this.model.toJSON()
          );
        }
      },

      initialize: function() {
        if (this.model) {
          this.model.on('change', this.render, this);
        }
        this.on('change', this.render, this);
      },

      template: _.template($('#template-config-overview').html()),

      render: _.throttle(function () {
        var cmd = this.model.get('commandName');
        if (cmd !== undefined && cmd !== 'home') {
          this.$el.html(this.template({c: TDWeb.Config.common,
                                       opts: TDWeb.Config.getOptions(cmd),
                                       optMap: TDWeb.Config.optionsMap}));
          this.$('button').button();
        } else {
          this.$el.html('');
        }
        return this;
      }, 100),
    });
    
    TDWeb.Views.ConfigEditor = TDWeb.Views.BaseView.extend({
      tagName: 'div',
      className: 'config-editor',

      events: {
        'submit .config-edit-form': 'saveButton',
        'change .option-name': 'toggleOption'
      },

      cancelButton: function() {
        this.$el.dialog('close');
        this.render();
        return;
      },

      saveButton: function() {
        var cmd = TDWeb.Config.common.get('commandName');
        var opts = TDWeb.Config.getOptions(cmd);
        var form = this.$('.config-edit-form').serializeArray();
        var allfields, enabled, disabled, type;

        enabled = _.map(
          _.pluck(
            _.filter(form,
              function(field) {
                return field.name.substr(0,7) == 'enable-';
              }),
            'name'
          ),
          function(str) { return str.substr(7); }
        );

        allfields = _.pick(
          _.object(
            _.pluck(form, 'name'),
            _.pluck(form, 'value')
          ),
          opts
        );

        if (_.contains(enabled,'padSize')) {
          allfields['padSize'] = _.chain(form).where({name:'padSize'})
                                  .pluck('value').value().join('');
        }

        disabled = _.without(opts, enabled);
        _.each(disabled, function(key) {
          TDWeb.Config.common.unset(key);
        });

        _.each(enabled, function(key) {
          if (allfields[key] !== undefined) {
            type = TDWeb.Config.optionsMap[key]['type'];
            switch(type) {
              case 'place':
              case 'station':
              case 'system':
                TDWeb.Data.getPlace(type, allfields[key], function(v) {
                  TDWeb.Config.common.set(key, v.id);
                });
                break;
              default:
                TDWeb.Config.common.set(key, allfields[key]);
            }
          } else {
            TDWeb.Config.common.unset(key);
          }
        });

        this.$el.dialog('close');
        return;
      },

      toggleOption: function(ev) {
        var $t = $(ev.currentTarget).find('input');
        var $e = $t.closest('tr').find('.option-value input');
        var $b = $t.closest('tr').find('.conf-buttonset input');
        if ($t.prop('checked')) {
          $e.prop('disabled', false);
          $b.button('enable');
        } else {
          $e.prop('disabled', true);
          $b.button('disable');
        }
      },

      initialize: function() {
        var that = this;
        
        if (this.model) {
          this.model.on('change', this.render, this);
        }
        this.$el.dialog({
          title: 'Configuration:',
          autoOpen: false,
          modal: true,
          closeOnEscape: true,
          hide: true,
          show: true,
          open: this.render,
          width: 500,
          height: 300,
          buttons: {
            Save: function() { that.saveButton(); },
            Cancel: function() { that.cancelButton(); }
          }
        });
      },

      template: _.template($('#template-config-editor').html()),

      render: function () {
        if (this.model === undefined || this.$el === undefined) {
          return this;
        }
        var cmd = this.model.get('commandName');
         
        this.$el.html(this.template({c: TDWeb.Config.common,
                                     opts: TDWeb.Config.getOptions(cmd),
                                     optMap: TDWeb.Config.optionsMap,
                                     cmd: cmd}));
        this.$el.hide();
        this.$('button').button();
        this.$('.conf-buttonset').buttonset();
        this.$('input.place').autocomplete({
          source: '/api/autocomplete/place/15'
        });
        this.$('input.station').autocomplete({
          source: '/api/autocomplete/station/15'
        });
        this.$('input.system').autocomplete({
          source: '/api/autocomplete/system/15'
        });
        this.$('input.item').autocomplete({
          source: '/api/autocomplete/item/15'
        });
        this.$('input.ship').autocomplete({
          source: '/api/autocomplete/ship/15'
        });
        return this;
      }
    });

    TDWeb.Views.ResultsView = TDWeb.Views.BaseView.extend({
      tagName: 'pre',
      className: 'results-view',

      initialize: function() {
        if (this.model) {
          this.model.on('change', this.render, this);
        }
      },

      template: _.template($('#template-results').html()),

      render: function () {
        var cmd = this.model.get('commandName');
        this.$el.html(this.template({
          results: JSON.stringify(this.model.toJSON(), undefined, 2)
        }));
        return this;
      }
    });


    TDWeb.Views.MenuBar = TDWeb.Views.BaseView.extend({
      events: {
        'focusout li': 'focusOff',
        'focusin li': 'focusOn',
        'mouseenter li': 'hoverOn',
        'mouseleave li': 'hoverOff'
      },

      hoverOn: function(ev) {
        $(ev.currentTarget).addClass('ui-state-hover');
      },
      hoverOff: function(ev) {
        $(ev.currentTarget).removeClass('ui-state-hover');
      },
      focusOn: function(ev) {
        $(ev.currentTarget).addClass('ui-state-focus');
      },
      focusOff: function(ev) {
        $(ev.currentTarget).removeClass('ui-state-focus');
      },

      setActive: function(route) {
        this.$('li').removeClass('ui-state-active');
        this.$('#menu-' + route.toLowerCase()).addClass('ui-state-active');
      },

      id: 'menubar',
      tagName: 'ul',
      className: 'ui-helper-reset ui-helper-clearfix '+
                 'ui-widget-header ui-corner-top',

      template: _.template($('#template-menubar').html()),

      render: function () {
        var width =  100.0 / ( TDWeb.commandList.length + 1.0 );
        this.$el.html(this.template({commands: TDWeb.commandList}));
        this.$('li').width(width+"%")
          .addClass('ui-state-default ui-corner-all menubar-button');
        return this;
      },

      initialize: function() {
        this.listenTo(TDWeb.router, 'route', this.setActive);
      }
    });


    ///////////////////////
    // Controllers
    ///////////////////////
    TDWeb.Controllers.Notifications = TDWeb.Models.BaseModel.extend({
      initialize: function () {
        this.stack = {
          dir1: "up",
          dir2: "right",
          spacing1: 0,
          spacing2: 0
        };

        this.opts = {
          addclass: "notification-bar",
          cornerclass: "",
          width: "70%",
          stack: this.stack,
          styling: 'jqueryui'
        };

        this.listenTo(
          Backbone,
          'notify:info',
          this.showInfo);
        this.listenTo(
          Backbone,
          'notify:error',
          this.showError);
        this.listenTo(
          Backbone,
          'notify:success',
          this.showSuccess);
        this.listenTo(
          Backbone,
          'notify:notice',
          this.showNotice);
        this.listenTo(
          Backbone,
          'notify:debug',
          this.showDebug);
      },

      showInfo: function (msg) {
        this.opts.type = 'info';
        this.opts.title = 'Info';
        this.opts.text = msg;
        TDWeb.debug_log("info: " + msg);
        var n = new PNotify(this.opts);
        n.get().click(function () {
          n.remove();
        });
      },

      showError: function (msg) {
        this.opts.type = 'error';
        this.opts.title = 'Error';
        this.opts.text = msg;
        TDWeb.debug_log("error: " + msg);
        var n = new PNotify(this.opts);
        n.get().click(function () {
          n.remove();
        });
      },

      showSuccess: function (msg) {
        this.opts.type = 'success';
        this.opts.title = 'Success';
        this.opts.text = msg;
        TDWeb.debug_log("success: " + msg);
        var n = new PNotify(this.opts);
        n.get().click(function () {
          n.remove();
        });
      },

      showNotice: function (msg) {
        this.opts.type = 'notice';
        this.opts.title = 'Notice';
        this.opts.text = msg;
        TDWeb.debug_log("notice: " + msg);
        var n = new PNotify(this.opts);
        n.get().click(function () {
          n.remove();
        });
      },

      showDebug: function (msg) {
        if (!TDWeb.debug) {return;}
        this.opts.type = 'info';
        this.opts.title = 'Info';
        this.opts.text = msg;
        TDWeb.debug_log("info: " + msg);
        var n = new PNotify(this.opts);
        n.get().click(function () {
          n.remove();
        });
      }

    });

    TDWeb.Views.menubar = new TDWeb.Views.MenuBar();
    $('header').append(TDWeb.Views.menubar.render().$el);

    TDWeb.Views.content = new TDWeb.Views.Content();
    TDWeb.Views.content.render();

    TDWeb.Views.confOverview = new TDWeb.Views.ConfigOverview(
      {model: TDWeb.Config.common}
    );
    $('#left-panel').append(TDWeb.Views.confOverview.$el);

    TDWeb.Views.confEditor = new TDWeb.Views.ConfigEditor(
      {model: TDWeb.Config.common}
    );

    TDWeb.Views.results = new TDWeb.Views.ResultsView(
      {model: TDWeb.Data.results}
    );
    $('#right-panel').append(TDWeb.Views.results.$el);

    TDWeb.Controllers.notifications = new TDWeb.Controllers.Notifications();
    TDWeb.init_routing();
    Backbone.trigger('notify:debug', 'routing initialized.');
  };

  /*---------------------------------------------------------------------------
   * Initialize and run.
   *-------------------------------------------------------------------------*/

   TDWeb.start();

  // Close encapsulation.
} ());
