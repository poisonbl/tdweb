// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
})();

// Place any jQuery/helper plugins in here.
/*---------------------------------------------------------------------------
 * Query string parsing. - mostly from Campbeln @ StackOverflow:
 * http://stackoverflow.com/a/23401756
 * Modified to ease parsing query strings from Backbone.js
 * Exports itself globally as window.QueryParser
 *-------------------------------------------------------------------------*/
(function (root, factory) {
  'use strict';
  root.QueryParser = factory(root,{})
})(this, function (root, QueryParser) {
  QueryParser.parse = function (q) {
    var b, cv, e, k, ma, sk, v, r = {},
    d = function (v) {
      return decodeURIComponent(v).replace(/\+/g, ' ');
    },
    //# d(ecode) the v(alue)
    s = /([^&;=]+)=?([^&;]*)/g;

    //# ma(make array) out of the v(alue)
    ma = function (v) {
      //# If the passed v(alue) hasn't been setup as an object
      if (typeof v != 'object') {
        //# Grab the cv(current value) then setup the v(alue) as an object
        cv = v;
        v = {};
        v.length = 0;

        //# If there was a cv(current value), .push it into the new
        //  v(alue)'s array
        if (cv) {
          Array.prototype.push.call(v, cv);
        }
      }
      return v;
    };

    //# While we still have key-value e(ntries) from the q(uerystring) via the
    //  s(earch regex)...
    while (e = s.exec(q)) { //# while((e = s.exec(q)) !== null) {
      //# Collect the open b(racket) location (if any) then set the
      //  d(ecoded) v(alue) from the above split key-value e(ntry)
      b = e[1].indexOf('[');
      v = d(e[2]);

      //# As long as this is NOT a hash[]-style key-value e(ntry)
      if (b < 0) { //# b == '-1'
        //# d(ecode) the simple k(ey)
        k = d(e[1]);

        //# If the k(ey) already exists
        if (r[k]) {
          //# ma(make array) out of the k(ey) then .push the v(alue) into the
          //  k(ey)'s array in the r(eturn value)
          r[k] = ma(r[k]);
          Array.prototype.push.call(r[k], v);
        }
        //# Else this is a new k(ey), so just add the k(ey)/v(alue) into
        //  the r(eturn value)
        else {
          r[k] = v;
        }
      }
      //# Else we've got ourselves a hash[]-style key-value e(ntry)
      else {
        //# Collect the d(ecoded) k(ey) and the d(ecoded) sk(sub-key) based
        //  on the b(racket) locations
        k = d(e[1].slice(0, b));
        sk = d(e[1].slice(b + 1, e[1].indexOf(']', b)));
        //# ma(make array) out of the k(ey)
        r[k] = ma(r[k]);

        //# If we have a sk(sub-key), plug the v(alue) into it
        if (sk) {
          r[k][sk] = v;
        }
        //# Else .push the v(alue) into the k(ey)'s array
        else {
          Array.prototype.push.call(r[k], v);
        }
      }
    }

    //# Return the r(eturn value)
    return r;
  };
  
  QueryParser.getKey = function (q, key) {
    return QueryParser.parse(q)[key];
  };
  
  return QueryParser;
});


/*---------------------------------------------------------------------------
 * String toTitleCase() - courtesy of Geoffrey Booth @ Stack Overflow
 * http://stackoverflow.com/a/6475125
 * Extends String.prototype directly.
 *-------------------------------------------------------------------------*/
(function () {
  'use strict';
  String.prototype.toTitleCase = function () {
    var i, j, s, ls, us;
    s = this.replace(/([^\W_]+[^\s-]*) */g, function (t) {
        return t.charAt(0).toUpperCase() + t.substr(1).toLowerCase();
      });

    // Certain minor words should be left lowercase unless
    // they are the first or last words in the string
    ls = ['A', 'An', 'The', 'And', 'But', 'Or', 'For', 'Nor', 'As', 'At',
      'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto',
      'To', 'With']
    for (i = 0, j = ls.length; i < j; i++)
      s = s.replace(new RegExp('\\s' + ls[i] + '\\s', 'g'),
          function (t) {
          return t.toLowerCase();
        });

    // Certain words such as initialisms or acronyms should be left uppercase
    us = ['Id', 'Tv'];
    for (i = 0, j = us.length; i < j; i++)
      s = s.replace(new RegExp('\\b' + us[i] + '\\b', 'g'),
          us[i].toUpperCase());

    return s;
  };
})();


(function(root, factory) {
  'use strict';
  root.LRUCache = factory(root, {});
})(this, function (root, LRUCache) {
  /**
   * A doubly linked list-based Least Recently Used (LRU) cache. Will keep most
   * recently used items while discarding least recently used items when its
   * limit is reached.
   *
   * Licensed under MIT. Copyright (c) 2010 Rasmus Andersson <http://hunch.se/>
   * See README.md for details.
   *
   * Illustration of the design:
   *
   *       entry             entry             entry             entry
   *       ______            ______            ______            ______
   *      | head |.newer => |      |.newer => |      |.newer => | tail |
   *      |  A   |          |  B   |          |  C   |          |  D   |
   *      |______| <= older.|______| <= older.|______| <= older.|______|
   *
   *  removed  <--  <--  <--  <--  <--  <--  <--  <--  <--  <--  <--  added
   */
  function LRUCache(limit) {
    // Current size of the cache. (Read-only).
    this.size = 0;
    // Maximum number of items this cache can hold.
    this.limit = limit;
    this._keymap = {};
  }

  /**
   * Put <value> into the cache associated with <key>. Returns the entry which
   * was removed to make room for the new entry. Otherwise undefined is
   * returned (i.e. if there was enough room already).
   */
  LRUCache.prototype.put = function (key, value) {
    var entry = {
      key : key,
      value : value
    };
    // Note: No protection against replacing, and thus orphan entries. By
    // design.
    this._keymap[key] = entry;
    if (this.tail) {
      // link previous tail to the new tail (entry)
      this.tail.newer = entry;
      entry.older = this.tail;
    } else {
      // we're first in -- yay
      this.head = entry;
    }
    // add new entry to the end of the linked list -- it's now the freshest
    // entry.
    this.tail = entry;
    if (this.size === this.limit) {
      // we hit the limit -- remove the head
      return this.shift();
    } else {
      // increase the size counter
      this.size++;
    }
  };

  /**
   * Purge the least recently used (oldest) entry from the cache. Returns the
   * removed entry or undefined if the cache was empty.
   *
   * If you need to perform any form of finalization of purged items, this is a
   * good place to do it. Simply override/replace this function:
   *
   *   var c = new LRUCache(123);
   *   c.shift = function() {
   *     var entry = LRUCache.prototype.shift.call(this);
   *     doSomethingWith(entry);
   *     return entry;
   *   }
   */
  LRUCache.prototype.shift = function () {
    // todo: handle special case when limit == 1
    var entry = this.head;
    if (entry) {
      if (this.head.newer) {
        this.head = this.head.newer;
        this.head.older = undefined;
      } else {
        this.head = undefined;
      }
      // Remove last strong reference to <entry> and remove links from the
      // entry being returned:
      entry.newer = entry.older = undefined;
      // delete is slow, but we need to do this to avoid uncontrollable growth:
      delete this._keymap[entry.key];
    }
    return entry;
  };

  /**
   * Get and register recent use of <key>. Returns the value associated with
   * <key> or undefined if not in cache.
   */
  LRUCache.prototype.get = function (key, returnEntry) {
    // First, find our cache entry
    var entry = this._keymap[key];
    if (entry === undefined)
      return; // Not cached. Sorry.
    // As <key> was found in the cache, register it as being requested recently
    if (entry === this.tail) {
      // Already the most recenlty used entry, so no need to update the list
      return returnEntry ? entry : entry.value;
    }
    // HEAD--------------TAIL
    //   <.older   .newer>
    //  <--- add direction --
    //   A  B  C  <D>  E
    if (entry.newer) {
      if (entry === this.head)
        this.head = entry.newer;
      entry.newer.older = entry.older; // C <-- E.
    }
    if (entry.older)
      entry.older.newer = entry.newer; // C. --> E
    entry.newer = undefined; // D --x
    entry.older = this.tail; // D. --> E
    if (this.tail)
      this.tail.newer = entry; // E. <-- D
    this.tail = entry;
    return returnEntry ? entry : entry.value;
  };

  /**
   * Check if <key> is in the cache without registering recent use. Feasible if
   * you do not want to change the state of the cache, but only "peek" at it.
   * Returns the entry associated with <key> if found, or undefined.
   */
  LRUCache.prototype.find = function (key) {
    return this._keymap[key];
  };

  /**
   * Update the value of entry with <key>. Returns the old value, or undefined
   * if entry was not in the cache.
   */
  LRUCache.prototype.set = function (key, value) {
    var oldvalue,
    entry = this.get(key, true);
    if (entry) {
      oldvalue = entry.value;
      entry.value = value;
    } else {
      oldvalue = this.put(key, value);
      if (oldvalue)
        oldvalue = oldvalue.value;
    }
    return oldvalue;
  };

  /**
   * Remove entry <key> from cache and return its value. Returns undefined if
   * not found.
   */
  LRUCache.prototype.remove = function (key) {
    var entry = this._keymap[key];
    if (!entry)
      return;
    delete this._keymap[entry.key]; // need to do delete unfortunately
    if (entry.newer && entry.older) {
      // relink the older entry with the newer entry
      entry.older.newer = entry.newer;
      entry.newer.older = entry.older;
    } else if (entry.newer) {
      // remove the link to us
      entry.newer.older = undefined;
      // link the newer entry to head
      this.head = entry.newer;
    } else if (entry.older) {
      // remove the link to us
      entry.older.newer = undefined;
      // link the newer entry to head
      this.tail = entry.older;
    } else { // if(entry.older === undefined && entry.newer === undefined) {
      this.head = this.tail = undefined;
    }

    this.size--;
    return entry.value;
  };

  /** Removes all entries */
  LRUCache.prototype.removeAll = function () {
    // This should be safe, as we never expose strong refrences to the outside
    this.head = this.tail = undefined;
    this.size = 0;
    this._keymap = {};
  };

  /**
   * Return an array containing all keys of entries stored in the cache object,
   * in arbitrary order.
   */
  if (typeof Object.keys === 'function') {
    LRUCache.prototype.keys = function () {
      return Object.keys(this._keymap);
    };
  } else {
    LRUCache.prototype.keys = function () {
      var keys = [];
      for (var k in this._keymap)
        keys.push(k);
      return keys;
    };
  }

  /**
   * Call `fun` for each entry. Starting with the newest entry if `desc` is a
   * true value, otherwise starts with the oldest (head) enrty and moves
   * towards the tail.
   *
   * `fun` is called with 3 arguments in the context `context`:
   *   `fun.call(context, Object key, Object value, LRUCache self)`
   */
  LRUCache.prototype.forEach = function (fun, context, desc) {
    var entry;
    if (context === true) {
      desc = true;
      context = undefined;
    } else if (typeof context !== 'object')
      context = this;
    if (desc) {
      entry = this.tail;
      while (entry) {
        fun.call(context, entry.key, entry.value, this);
        entry = entry.older;
      }
    } else {
      entry = this.head;
      while (entry) {
        fun.call(context, entry.key, entry.value, this);
        entry = entry.newer;
      }
    }
  };

  /** Returns a String representation */
  LRUCache.prototype.toString = function () {
    var s = '',
    entry = this.head;
    while (entry) {
      s += String(entry.key) + ':' + entry.value;
      entry = entry.newer;
      if (entry)
        s += ' < ';
    }
    return s;
  };

  return LRUCache;
});

/*
PNotify 2.0.1 sciactive.com/pnotify/
(C) 2014 Hunter Perrin
license GPL/LGPL/MPL
*/
(function(c){'use strict';"function"===typeof define&&define.amd?define("pnotify",["jquery"],c):c(jQuery)})(function(c){var p={dir1:"down",dir2:"left",push:"bottom",spacing1:25,spacing2:25,context:c("body")},f,g,h=c(window),m=function(){g=c("body");PNotify.prototype.options.stack.context=g;h=c(window);h.bind("resize",function(){f&&clearTimeout(f);f=setTimeout(function(){PNotify.positionAll(!0)},10)})};PNotify=function(b){this.parseOptions(b);this.init()};c.extend(PNotify.prototype,{version:"2.0.1",options:{title:!1,
title_escape:!1,text:!1,text_escape:!1,styling:"bootstrap3",addclass:"",cornerclass:"",auto_display:!0,width:"300px",min_height:"16px",type:"notice",icon:!0,opacity:1,animation:"fade",animate_speed:"slow",position_animate_speed:500,shadow:!0,hide:!0,delay:8E3,mouse_reset:!0,remove:!0,insert_brs:!0,destroy:!0,stack:p},modules:{},runModules:function(b,a){var c,e;for(e in this.modules)if(c="object"===typeof a&&e in a?a[e]:a,"function"===typeof this.modules[e][b])this.modules[e][b](this,"object"===typeof this.options[e]?
this.options[e]:{},c)},state:"initializing",timer:null,styles:null,elem:null,container:null,title_container:null,text_container:null,animating:!1,timerHide:!1,init:function(){var b=this;this.modules={};c.extend(!0,this.modules,PNotify.prototype.modules);this.styles="object"===typeof this.options.styling?this.options.styling:PNotify.styling[this.options.styling];this.elem=c("<div />",{"class":"ui-pnotify "+this.options.addclass,css:{display:"none"},mouseenter:function(a){if(b.options.mouse_reset&&
"out"===b.animating){if(!b.timerHide)return;b.cancelRemove()}b.options.hide&&b.options.mouse_reset&&b.cancelRemove()},mouseleave:function(a){b.options.hide&&b.options.mouse_reset&&b.queueRemove();PNotify.positionAll()}});this.container=c("<div />",{"class":this.styles.container+" ui-pnotify-container "+("error"===this.options.type?this.styles.error:"info"===this.options.type?this.styles.info:"success"===this.options.type?this.styles.success:this.styles.notice)}).appendTo(this.elem);""!==this.options.cornerclass&&
this.container.removeClass("ui-corner-all").addClass(this.options.cornerclass);this.options.shadow&&this.container.addClass("ui-pnotify-shadow");!1!==this.options.icon&&c("<div />",{"class":"ui-pnotify-icon"}).append(c("<span />",{"class":!0===this.options.icon?"error"===this.options.type?this.styles.error_icon:"info"===this.options.type?this.styles.info_icon:"success"===this.options.type?this.styles.success_icon:this.styles.notice_icon:this.options.icon})).prependTo(this.container);this.title_container=
c("<h4 />",{"class":"ui-pnotify-title"}).appendTo(this.container);!1===this.options.title?this.title_container.hide():this.options.title_escape?this.title_container.text(this.options.title):this.title_container.html(this.options.title);this.text_container=c("<div />",{"class":"ui-pnotify-text"}).appendTo(this.container);!1===this.options.text?this.text_container.hide():this.options.text_escape?this.text_container.text(this.options.text):this.text_container.html(this.options.insert_brs?String(this.options.text).replace(/\n/g,
"<br />"):this.options.text);"string"===typeof this.options.width&&this.elem.css("width",this.options.width);"string"===typeof this.options.min_height&&this.container.css("min-height",this.options.min_height);PNotify.notices="top"===this.options.stack.push?c.merge([this],PNotify.notices):c.merge(PNotify.notices,[this]);"top"===this.options.stack.push&&this.queuePosition(!1,1);this.options.stack.animation=!1;this.runModules("init");this.options.auto_display&&this.open();return this},update:function(b){var a=
this.options;this.parseOptions(a,b);this.options.cornerclass!==a.cornerclass&&this.container.removeClass("ui-corner-all "+a.cornerclass).addClass(this.options.cornerclass);this.options.shadow!==a.shadow&&(this.options.shadow?this.container.addClass("ui-pnotify-shadow"):this.container.removeClass("ui-pnotify-shadow"));!1===this.options.addclass?this.elem.removeClass(a.addclass):this.options.addclass!==a.addclass&&this.elem.removeClass(a.addclass).addClass(this.options.addclass);!1===this.options.title?
this.title_container.slideUp("fast"):this.options.title!==a.title&&(this.options.title_escape?this.title_container.text(this.options.title):this.title_container.html(this.options.title),!1===a.title&&this.title_container.slideDown(200));!1===this.options.text?this.text_container.slideUp("fast"):this.options.text!==a.text&&(this.options.text_escape?this.text_container.text(this.options.text):this.text_container.html(this.options.insert_brs?String(this.options.text).replace(/\n/g,"<br />"):this.options.text),
!1===a.text&&this.text_container.slideDown(200));this.options.type!==a.type&&this.container.removeClass(this.styles.error+" "+this.styles.notice+" "+this.styles.success+" "+this.styles.info).addClass("error"===this.options.type?this.styles.error:"info"===this.options.type?this.styles.info:"success"===this.options.type?this.styles.success:this.styles.notice);if(this.options.icon!==a.icon||!0===this.options.icon&&this.options.type!==a.type)this.container.find("div.ui-pnotify-icon").remove(),!1!==this.options.icon&&
c("<div />",{"class":"ui-pnotify-icon"}).append(c("<span />",{"class":!0===this.options.icon?"error"===this.options.type?this.styles.error_icon:"info"===this.options.type?this.styles.info_icon:"success"===this.options.type?this.styles.success_icon:this.styles.notice_icon:this.options.icon})).prependTo(this.container);this.options.width!==a.width&&this.elem.animate({width:this.options.width});this.options.min_height!==a.min_height&&this.container.animate({minHeight:this.options.min_height});this.options.opacity!==
a.opacity&&this.elem.fadeTo(this.options.animate_speed,this.options.opacity);this.options.hide?a.hide||this.queueRemove():this.cancelRemove();this.queuePosition(!0);this.runModules("update",a);return this},open:function(){this.state="opening";this.runModules("beforeOpen");var b=this;this.elem.parent().length||this.elem.appendTo(this.options.stack.context?this.options.stack.context:g);"top"!==this.options.stack.push&&this.position(!0);"fade"===this.options.animation||"fade"===this.options.animation.effect_in?
this.elem.show().fadeTo(0,0).hide():1!==this.options.opacity&&this.elem.show().fadeTo(0,this.options.opacity).hide();this.animateIn(function(){b.queuePosition(!0);b.options.hide&&b.queueRemove();b.state="open";b.runModules("afterOpen")});return this},remove:function(b){this.state="closing";this.timerHide=!!b;this.runModules("beforeClose");var a=this;this.timer&&(window.clearTimeout(this.timer),this.timer=null);this.animateOut(function(){a.state="closed";a.runModules("afterClose");a.queuePosition(!0);
a.options.remove&&a.elem.detach();a.runModules("beforeDestroy");if(a.options.destroy&&null!==PNotify.notices){var b=c.inArray(a,PNotify.notices);-1!==b&&PNotify.notices.splice(b,1)}a.runModules("afterDestroy")});return this},get:function(){return this.elem},parseOptions:function(b,a){this.options=c.extend(!0,{},PNotify.prototype.options);this.options.stack=PNotify.prototype.options.stack;var n=[b,a],e,f;for(f in n){e=n[f];if("undefined"==typeof e)break;if("object"!==typeof e)this.options.text=e;else for(var d in e)this.modules[d]?
c.extend(!0,this.options[d],e[d]):this.options[d]=e[d]}},animateIn:function(b){this.animating="in";var a;a="undefined"!==typeof this.options.animation.effect_in?this.options.animation.effect_in:this.options.animation;"none"===a?(this.elem.show(),b()):"show"===a?this.elem.show(this.options.animate_speed,b):"fade"===a?this.elem.show().fadeTo(this.options.animate_speed,this.options.opacity,b):"slide"===a?this.elem.slideDown(this.options.animate_speed,b):"function"===typeof a?a("in",b,this.elem):this.elem.show(a,
"object"===typeof this.options.animation.options_in?this.options.animation.options_in:{},this.options.animate_speed,b);this.elem.parent().hasClass("ui-effects-wrapper")&&this.elem.parent().css({position:"fixed",overflow:"visible"});"slide"!==a&&this.elem.css("overflow","visible");this.container.css("overflow","hidden")},animateOut:function(b){this.animating="out";var a;a="undefined"!==typeof this.options.animation.effect_out?this.options.animation.effect_out:this.options.animation;"none"===a?(this.elem.hide(),
b()):"show"===a?this.elem.hide(this.options.animate_speed,b):"fade"===a?this.elem.fadeOut(this.options.animate_speed,b):"slide"===a?this.elem.slideUp(this.options.animate_speed,b):"function"===typeof a?a("out",b,this.elem):this.elem.hide(a,"object"===typeof this.options.animation.options_out?this.options.animation.options_out:{},this.options.animate_speed,b);this.elem.parent().hasClass("ui-effects-wrapper")&&this.elem.parent().css({position:"fixed",overflow:"visible"});"slide"!==a&&this.elem.css("overflow",
"visible");this.container.css("overflow","hidden")},position:function(b){var a=this.options.stack,c=this.elem;c.parent().hasClass("ui-effects-wrapper")&&(c=this.elem.css({left:"0",top:"0",right:"0",bottom:"0"}).parent());"undefined"===typeof a.context&&(a.context=g);if(a){"number"!==typeof a.nextpos1&&(a.nextpos1=a.firstpos1);"number"!==typeof a.nextpos2&&(a.nextpos2=a.firstpos2);"number"!==typeof a.addpos2&&(a.addpos2=0);var e="none"===c.css("display");if(!e||b){var f,d={},k;switch(a.dir1){case "down":k=
"top";break;case "up":k="bottom";break;case "left":k="right";break;case "right":k="left"}b=parseInt(c.css(k).replace(/(?:\..*|[^0-9.])/g,""));isNaN(b)&&(b=0);"undefined"!==typeof a.firstpos1||e||(a.firstpos1=b,a.nextpos1=a.firstpos1);var l;switch(a.dir2){case "down":l="top";break;case "up":l="bottom";break;case "left":l="right";break;case "right":l="left"}f=parseInt(c.css(l).replace(/(?:\..*|[^0-9.])/g,""));isNaN(f)&&(f=0);"undefined"!==typeof a.firstpos2||e||(a.firstpos2=f,a.nextpos2=a.firstpos2);
if("down"===a.dir1&&a.nextpos1+c.height()>(a.context.is(g)?h.height():a.context.prop("scrollHeight"))||"up"===a.dir1&&a.nextpos1+c.height()>(a.context.is(g)?h.height():a.context.prop("scrollHeight"))||"left"===a.dir1&&a.nextpos1+c.width()>(a.context.is(g)?h.width():a.context.prop("scrollWidth"))||"right"===a.dir1&&a.nextpos1+c.width()>(a.context.is(g)?h.width():a.context.prop("scrollWidth")))a.nextpos1=a.firstpos1,a.nextpos2+=a.addpos2+("undefined"===typeof a.spacing2?25:a.spacing2),a.addpos2=0;if(a.animation&&
a.nextpos2<f)switch(a.dir2){case "down":d.top=a.nextpos2+"px";break;case "up":d.bottom=a.nextpos2+"px";break;case "left":d.right=a.nextpos2+"px";break;case "right":d.left=a.nextpos2+"px"}else"number"===typeof a.nextpos2&&c.css(l,a.nextpos2+"px");switch(a.dir2){case "down":case "up":c.outerHeight(!0)>a.addpos2&&(a.addpos2=c.height());break;case "left":case "right":c.outerWidth(!0)>a.addpos2&&(a.addpos2=c.width())}if("number"===typeof a.nextpos1)if(a.animation&&(b>a.nextpos1||d.top||d.bottom||d.right||
d.left))switch(a.dir1){case "down":d.top=a.nextpos1+"px";break;case "up":d.bottom=a.nextpos1+"px";break;case "left":d.right=a.nextpos1+"px";break;case "right":d.left=a.nextpos1+"px"}else c.css(k,a.nextpos1+"px");(d.top||d.bottom||d.right||d.left)&&c.animate(d,{duration:this.options.position_animate_speed,queue:!1});switch(a.dir1){case "down":case "up":a.nextpos1+=c.height()+("undefined"===typeof a.spacing1?25:a.spacing1);break;case "left":case "right":a.nextpos1+=c.width()+("undefined"===typeof a.spacing1?
25:a.spacing1)}}return this}},queuePosition:function(b,a){f&&clearTimeout(f);a||(a=10);f=setTimeout(function(){PNotify.positionAll(b)},a);return this},cancelRemove:function(){this.timer&&window.clearTimeout(this.timer);"closing"===this.state&&(this.elem.stop(!0),this.state="open",this.animating="in",this.elem.css("height","auto").animate({width:this.options.width,opacity:this.options.opacity},"fast"));return this},queueRemove:function(){var b=this;this.cancelRemove();this.timer=window.setTimeout(function(){b.remove(!0)},
isNaN(this.options.delay)?0:this.options.delay);return this}});c.extend(PNotify,{notices:[],removeAll:function(){c.each(PNotify.notices,function(){this.remove&&this.remove()})},positionAll:function(b){f&&clearTimeout(f);f=null;c.each(PNotify.notices,function(){var a=this.options.stack;a&&(a.nextpos1=a.firstpos1,a.nextpos2=a.firstpos2,a.addpos2=0,a.animation=b)});c.each(PNotify.notices,function(){this.position()})},styling:{jqueryui:{container:"ui-widget ui-widget-content ui-corner-all",notice:"ui-state-highlight",
notice_icon:"ui-icon ui-icon-info",info:"",info_icon:"ui-icon ui-icon-info",success:"ui-state-default",success_icon:"ui-icon ui-icon-circle-check",error:"ui-state-error",error_icon:"ui-icon ui-icon-alert"},bootstrap2:{container:"alert",notice:"",notice_icon:"icon-exclamation-sign",info:"alert-info",info_icon:"icon-info-sign",success:"alert-success",success_icon:"icon-ok-sign",error:"alert-error",error_icon:"icon-warning-sign"},bootstrap3:{container:"alert",notice:"alert-warning",notice_icon:"glyphicon glyphicon-exclamation-sign",
info:"alert-info",info_icon:"glyphicon glyphicon-info-sign",success:"alert-success",success_icon:"glyphicon glyphicon-ok-sign",error:"alert-danger",error_icon:"glyphicon glyphicon-warning-sign"}}});PNotify.styling.fontawesome=c.extend({},PNotify.styling.bootstrap3);c.extend(PNotify.styling.fontawesome,{notice_icon:"fa fa-exclamation-circle",info_icon:"fa fa-info",success_icon:"fa fa-check",error_icon:"fa fa-warning"});document.body?m():c(m);return PNotify});