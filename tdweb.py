#!/usr/bin/env python
# TradeDangerous :: WebApp :: Test

import os
import sys
import json
try:
    import bottle
except ImportError:
    print("TDWeb: bottle.py is required. Please see the readme.")
    exit(1)

class TDWeb(object):
    def __init__(self, addRoutes=False, ui=False,
                 tdenv=None, tdb=None, conf="tdweb.conf"):
        self.app = bottle.Bottle()
        c = self.app.config
        if conf:
            c.load_config(conf)

        self.webroot = c.get("tdweb.webroot", "./static/")

        sys.path.append(os.path.realpath(
            c.get("tdweb.tdroot", "../tradedangerous")
        ))

        # Now that we have a path for TD, grab what we need
        # and initialize it.
        from tradeenv import TradeEnv
        from tradeexcept import TradeException
        from tradedb import TradeDB, AmbiguityError
        import commands
        from web_commands import WebCommandIndex

        self.AmbiguityError = AmbiguityError

        if not tdenv:
            self.tdenv = TradeEnv(
                dataDir=c.get("tdweb.tddata", "../tradedangerous/data")
            )
        else:
            self.tdenv = tdenv

        if not tdb:
            self.tdb = TradeDB(self.tdenv, load=False)
            self.tdb.load()
        else:
            self.tdb = tdb

        c.update({
            "tdenv": self.tdenv,
            "tdb": self.tdb,
            "ui": ui,
            "addRoutes": addRoutes,
        })

        if addRoutes:
            self.commandlist = [ "run", "buy", "sell", "rares", "nav",
                                 "local", "station", "system", "data", 
                                 "trade" ]
            wci = WebCommandIndex()
            self.set_routes(ui=ui)
            for cmd in wci.get_commands():
                setup = wci.setup_routes(cmd,self.app,False)


    def start(self, host=None, port=None, reloader=None):
        c = self.app.config
        if not host:
            host = c.get("tdweb.host", "127.0.0.1")
        if not port:
            port = c.get("tdweb.port", 8888)
        if not reloader:
            reloader = c.get("tdweb.debug", False)
        self.app.run(host=host, port=port, reloader=reloader)


    def set_routes(self,ui=True):
        # Core API handlers.
        self.app.route(
            "/api/autocomplete",
            method="GET", callback=self.get_autocomplete
        )
        self.app.route(
            "/api/autocomplete/<mode>",
            method="GET", callback=self.get_autocomplete
        )
        self.app.route(
            "/api/autocomplete/<mode>/<count>",
            method="GET", callback=self.get_autocomplete
        )

        # Items (incl. categories), Ships
        self.app.route(
            "/api/items",
            method="GET", callback=self.get_item
        )
        self.app.route(
            "/api/items/<item>",
            method="GET", callback=self.get_item
        )
        self.app.route(
            "/api/ships",
            method="GET", callback=self.get_ship
        )
        self.app.route(
            "/api/ships/<ship>",
            method="GET", callback=self.get_ship
        )

        # Places, Systems, Stations
        self.app.route(
            "/api/places/<place:path>",
            method="GET", callback=self.get_place
        )
        self.app.route(
            "/api/systems/<system>",
            method="GET", callback=self.get_system
        )
        self.app.route(
            "/api/stations/<system>/<station>",
            method="GET", callback=self.get_station
        )
        self.app.route(
            "/api/stations/<system>/<station>/prices",
            method="GET", callback=self.get_station_prices
        )
        self.app.route(
            "/api/stations/<station:path>/prices",
            method=["PUT","POST"], callback=self.post_station_prices
        )

        if ui:
            # Static files.
            self.app.route(
                "/js/<filename:re:.*\.js>",
                method="GET", callback=self.get_js
            )
            self.app.route(
                "/css/<filename:re:.*\.css>",
                method="GET", callback=self.get_css
            )
            self.app.route(
                "/images/<filename:re:.*\.png>",
                method="GET", callback=self.get_img
            )
            self.app.route(
                "/favicon.ico",
                method="GET", callback=self.get_favicon
            )

            # Dynamic UI/Frontend
            self.app.route("/", method="GET", callback=self.get_root)
            for c in self.commandlist:
                self.app.route("/" + c, method="GET", callback=self.get_root)

    def get_autocomplete(self, mode="place", count=5):
        q = bottle.request.params.get("term")
        try:
            count = int(count)
        except:
            return TDWeb.pretty_json_dump([ ])

        try:
        # Search the database, defaulting for generic "place"
        # if we get a successful hit, just return it.
            search = { "station": self.tdb.lookupStation,
                       "system": self.tdb.lookupSystem,
                       "place": self.tdb.lookupPlace,
                       "ship": self.tdb.lookupShip,
                       "category": self.tdb.lookupCategory,
                       "item": self.tdb.lookupItem }
            try:
                r = search[mode](q).name()
            except KeyError:
                r = self.tdb.lookupPlace(q).name()

            return TDWeb.pretty_json_dump([ r ])

        except self.AmbiguityError as e:
            # Multiple matches found. Limit to "count" items.
            n = min(len(e.anyMatch), count)
            r = [ ]
            for i in range(min(len(e.anyMatch), n)):
                if mode == "place":
                    r.append(e.anyMatch[i].name())
                else:
                    r.append(e.anyMatch[i][1].name())
            return TDWeb.pretty_json_dump(r)
        except LookupError:
            return TDWeb.pretty_json_dump([ ])


    # -------------------------------------------------------------------------
    # Static File Handlers
    # -------------------------------------------------------------------------
    def get_js(self, filename):
        return bottle.static_file(filename, root=self.webroot + "/js")

    def get_css(self, filename):
        return bottle.static_file(filename, root=self.webroot + "/css")

    def get_img(self, filename):
        return bottle.static_file(filename, root=self.webroot + "/images")

    def get_favicon(self):
        return bottle.static_file("favicon.ico", root=self.webroot + "/images")


    # -------------------------------------------------------------------------
    # Dynamic UI/Frontend Handler(s)
    # -------------------------------------------------------------------------
    def get_root(self, any=None):
        debug = self.app.config.get("tdweb.debug", False)
        if debug:
            print("Clearing templates.")
            bottle.TEMPLATES.clear()
        return bottle.template(
            "base", {"title": "TDWeb", "debug": debug}
        )


    # -------------------------------------------------------------------------
    # Backend API Handlers
    # -------------------------------------------------------------------------
    def get_item(self, item=None):
        if not item:
            items = self.tdb.items()
            r = [ ]
            for i in items:
                r.append(TDWeb.item_to_dict(i, self.tdb))
            return TDWeb.pretty_json_dump(r)
        try:
            i = self.tdb.lookupItem(item)
            return TDWeb.pretty_json_dump(TDWeb.item_to_dict(i, self.tdb))
        except self.AmbiguityError as e:
            bottle.response.status = 303
            return TDWeb.pretty_json_dump(
                TDWeb.ambiguity_error_to_dict(e)
            )
        except Exception as e:
            bottle.response.status = 400
            return TDWeb.pretty_json_dump({"error": repr(e)})

    def get_ship(self, ship=None):
        if not ship:
            ships = self.tdb.ships()
            r = [ ]
            for s in ships:
                r.append(TDWeb.item_to_dict(s, self.tdb))
            return TDWeb.pretty_json_dump(r)
        try:
            s = self.tdb.lookupShip(ship)
            return TDWeb.pretty_json_dump(TDWeb.item_to_dict(s, self.tdb))
        except self.AmbiguityError as e:
            bottle.response.status = 303
            return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
        except Exception as e:
            bottle.response.status = 400
            return TDWeb.pretty_json_dump({"error": repr(e)})

    def get_place(self, place=None):
        try:
            plc = self.tdb.lookupPlace(place)
            try:
                return TDWeb.pretty_json_dump(TDWeb.station_to_dict(plc))
            except:
                return TDWeb.pretty_json_dump(TDWeb.system_to_dict(plc))
        except self.AmbiguityError as e:
            bottle.response.status = 303
            return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
        except Exception as e:
            bottle.response.status = 400
            return TDWeb.pretty_json_dump({"error": repr(e)})

    def get_system(self, system):
        try:
            sys = self.tdb.lookupSystem(system)
            return TDWeb.pretty_json_dump(TDWeb.system_to_dict(sys))
        except self.AmbiguityError as e:
            bottle.response.status = 303
            return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
        except Exception as e:
            bottle.response.status = 400
            return TDWeb.pretty_json_dump({"error": repr(e)})

    def get_station(self, system=None, station=None, place=None):
        try:
            if place:
                stn = self.tdb.lookupStation(self.tdb.lookupPlace(place))
            else:
                stn = self.tdb.lookupStation(station, system=system)
            return TDWeb.pretty_json_dump(TDWeb.station_to_dict(stn))
        except self.AmbiguityError as e:
            bottle.response.status = 303
            return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
        except Exception as e:
            bottle.response.status = 400
            return TDWeb.pretty_json_dump({"error": repr(e)})

    def get_station_prices(self, system, station):
        from jsonprices import generate_prices_json
        try:
            stn = self.tdb.lookupStation(name=station, system=system)
            #station = self.tdb.lookupStation(station)
            return generate_prices_json(self.tdb, self.tdenv, stn)
        except self.AmbiguityError as e:
            bottle.response.status = 303
            return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
        except Exception as e:
            bottle.response.status = 400
            return TDWeb.pretty_json_dump({"error": repr(e)})

    def post_station_prices(self, station):
        return TDWeb.pretty_json_dump({ })



    @staticmethod
    def ambiguity_error_to_dict(error):
        matches = [ ]
        for match in error.anyMatch:
            try:
                matches.append(match.value.name())
            except:
                matches.append(match.name())
        err_str = "Ambiguous {} name: '{}' matches {} possibilities.".format(
            error.lookupType, error.searchKey, len(matches)
        )
        return { "error": err_str, "matches": matches }

    @staticmethod
    def system_to_dict(sys, _subcall=False):
        stns = [ ]
        for stn in sys.stations:
            if _subcall:
                stns.append(stn.name())
            else:
                stns.append(TDWeb.station_to_dict(stn, _subcall=True))

        return {
            "id": sys.dbname.upper(),
            "name": sys.dbname,
            "posX": sys.posX,
            "posY": sys.posY,
            "posZ": sys.posZ,
            "stations": stns
        }

    @staticmethod
    def station_to_dict(stn, _subcall=False):
        if _subcall:
            sys = stn.system.name()
        else:
            sys = TDWeb.system_to_dict(stn.system, _subcall=True)

        return {
            "id": stn.name(),
            "name": stn.dbname,
            "system": sys,
            "lsFromStar": stn.lsFromStar,
            "blackMarket": stn.blackMarket,
            "maxPadSize": stn.maxPadSize,
            "itemCount": stn.itemCount,
            "dataAge": stn.dataAge,
        }

    @staticmethod
    def item_to_dict(i, tdb):
        from tradedb import Item, RareItem, Ship

        if isinstance(i, RareItem):
            out = {"name": i.name(), "category": "RARE", "cost": i.costCr,
                   "maxAlloc":
                        "{:n}".format(i.maxAlloc) if i.maxAlloc >= 0 else "?",
                   "station": TDWeb.station_to_dict(i.station,True)}
        elif isinstance(i, Ship):
            out = {"name": i.name(), "category": "Ship", "cost": i.cost}
        elif isinstance(i, Item):
            avgSell = tdb.getAverageSelling()
            avgBuy = tdb.getAverageBuying()
            out = {
                "id":i.name(), "name": i.dbname,
                "average_buy": avgBuy[i.ID],
                "average_sell": avgSell[i.ID],
                "category": i.category.name(),
            }
        else:
            out = {}
        return out
    
    @staticmethod
    def pretty_json_dump(obj):
        return json.dumps(obj, sort_keys=True, indent=2,
                          separators=(",", ": "))


# -----------------------------------------------------------------------------
# Start the service
# -----------------------------------------------------------------------------
#
# At the end of the day, we're really just running a server with all of this,
# right?
if __name__ == "__main__":
    tdw = TDWeb(addRoutes=True, ui=True)
    tdw.start()
