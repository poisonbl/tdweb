% setdefault('title','TD-Web')
% setdefault('base','<div class="loader" style="margin-top:5em;">' +
%                   '  Loading...' +
%                   '</div>')

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{title}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/jquery-ui.min.css">
        <link rel="stylesheet" href="/css/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="/css/boilerplate.css">
        <link rel="stylesheet" href="/css/plugins.css">
        <link rel="stylesheet" href="/css/main.css">
% include('underscore.tpl')
        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <header>
        </header>

        <section id="content">{{!base}}</section>

        <footer>
        </footer>

        <script type="text/javascript" src="/js/vendor/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="/js/vendor/jquery-ui-1.11.2.min.js"></script>
        <script type="text/javascript" src="/js/vendor/underscore-1.7.0.min.js"></script>
        <script type="text/javascript" src="/js/vendor/backbone-1.1.2.min.js"></script>

        <script type="text/javascript" src="/js/plugins.js"></script>
        <script type="text/javascript" src="/js/main.js"></script>
    </body>
</html>