<script type="text/template" id="template-blank">
</script>

<script type="text/template" id="template-base">
  <div id="left-panel" class="ui-widget-header">
  </div>
  <div id="right-panel" class="ui-widget-content">
  </div>
</script>

<script type="text/template" id="template-menubar">
  <li id="menu-home"><a href="/">home</a></li>
  <@ _.each(commands, function(c) { @>
  <li id="menu-<@= c @>"><a href="/<@= c @>"><@= c @></a></li>
  <@ }); @>
</script>

<script type="text/template" id="template-config-overview">
  <thead>
    <tr>
      <th colspan="2">
        <button class="edit-button" type="button">edit</button>
        <button class="run-button" type="button">run</button>
      </th>
    </tr>
  </thead>
  <tbody>
  <@ _.each(opts, function(opt) { @>
    <tr>
      <td><@= optMap[opt].friendly @>:</td>
      <td><@= c.get(opt) !== undefined? c.get(opt) : 'n/a' @></td>
    </tr>
  <@ }); @>
  </tbody>
</script>

<script type="text/template" id="template-config-editor">
  <div class="config-edit-scroll">
    <form class="config-edit-form">
      <table>
      <@ var v, d;
         _.each(opts, function(opt) {
           v = c.get(opt) !== undefined? c.get(opt) : '';
           d = ''; @>
        <tr class="option" id="<@= opt @>">
          <td class="option-name">
            <input type="checkbox" name="enable-<@= opt @>" value="true" <@
              if (v !== '') {
                print(' checked');
              } else {
                d = ' disabled';
              }
            @>/>
            <label for="<@= opt @>"><@= optMap[opt].friendly @>:</label>
          </td>
          <td class="option-value">
            <@
            switch (optMap[opt].type) {
              case 'bool':
                print('<input type="checkbox" name="'+
                       opt + '" value="true"');
                if (v == 'true') {
                  print(' checked');
                }
                print(d+'/>');
                break;
              case 'float':
                print('<input type="number" step="0.01" class="float" name="'+
                       opt + '" value="'+ v +'"'+d+'/>');
                break;
              case 'int':
                print('<input type="number" step="1.0" class="int" name="'+
                       opt + '" value="'+ v +'"'+d+'/>');
                break;
              case 'item':
                print('<input type="text" class="item" name="'+
                       opt + '" value="'+ v +'"'+d+'/>');
                break;
              case 'list':
                print('<input type="text" name="' + opt + '" value="'+
                       v + '"'+d+'/>');
                break;
              case 'padsize':
                print('<div class="conf-buttonset">');
                print('<label for="pad_s">S</label>'+
                      '<input type="checkbox" name="padSize" id="pad_s" '+
                      'class="padsize pad_s" value="S"');
                if (v.indexOf('S') > -1) {
                  print(' checked');
                }
                print(d+'/>');
                print('<label for="pad_m">M</label>'+
                      '<input type="checkbox" name="padSize" id="pad_m" '+
                      'class="padsize pad_m" value="M"');
                if (v.indexOf('M') > -1) {
                  print(' checked');
                }
                print(d+'/>');
                print('<label for="pad_l">L</label>'+
                      '<input type="checkbox" name="padSize" id="pad_l" '+
                      'class="padsize pad_l" value="L"');
                if (v.indexOf('L') > -1) {
                  print(' checked');
                }
                print(d+'/>');
                print('<label for="pad_u">?</label>'+
                      '<input type="checkbox" name="padSize" id="pad_u" '+
                      'class="padsize pad_u" value="?"');
                if (v.indexOf('?') > -1) {
                  print(' checked');
                }
                print(d+'/>');
                print('</div>');
                break;
              case 'place': // to/from inputs
                print('<input type="text" class="place" name="'+
                       opt + '" value="'+ v +'"'+d+'/>');
                break;
              case 'station': // toStn/fromStn inputs
                print('<input type="text" class="station" name="'+
                       opt + '" value="'+ v +'"'+d+'/>');
                break;
              case 'sort': // distance, price, or (buy only) stock
                print('<div class="conf-buttonset">');
                print('<input type="radio" name="' + opt +
                      '" value="sortByDistance" id="sort_d"');
                if (v == 'sortByDistance') {
                  print(' checked');
                }
                print(d+'/><label for="sort_d">Distance</label>');
                print('<input type="radio" name="' + opt +
                      '" value="sortByPrice" id="sort_p"');
                if (v == 'sortByPrice') {
                  print(' checked');
                }
                print(d+'/><label for="sort_p">Price</label>');
                if (cmd == 'buy') {
                  print('<input type="radio" name="' + opt +
                        '" value="sortByStock" id="sort_s"');
                  if (v == 'sortByStock') {
                    print(' checked');
                  }
                  print(d+'/><label for="sort_s">Stock</label>');
                }
                print('</div>');
                break;
              case 'system': // near input
                print('<input type="text" class="system" name="'+
                      opt + '" value="'+ v +'"'+d+'/>');
                break;
              default:
                print('underscore.tpl: #template-config-editor got hungry...');
            }
            @></td>
        </tr>
      <@ }); @>
      </table>
    </form>
  </div>
</script>

<script type="text/template" id="template-results"><@= results @></script>