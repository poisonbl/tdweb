import web_commands
#import web_commands.buildcache_cmd
import web_commands.buy_cmd
#import web_commands.export_cmd
#import web_commands.import_cmd
import web_commands.local_cmd
import web_commands.nav_cmd
#import web_commands.olddata_cmd
import web_commands.rares_cmd
import web_commands.run_cmd
import web_commands.sell_cmd
#import web_commands.station_cmd
import web_commands.trade_cmd
#import web_commands.update_cmd

import commands

web_commandIndex = {
    cmd[0:cmd.find("_cmd")]: getattr(web_commands, cmd)
    for cmd in web_commands.__dir__() if cmd.endswith("_cmd")
}

class WebCommandIndex(object):
    def get_renderer(self,name):
        return web_commandIndex[name].render

    def setup_routes(self,name,app,ui=False):
        return web_commandIndex[name].setup_routes(app,ui)

    def get_commands(self):
        return web_commandIndex.keys()