import bottle
from tradeexcept import TradeException
from web_commands import tdweb_run
from tradedb import AmbiguityError
from tdweb import TDWeb

######################################################################
# Header
name = "local"
longname = "Local Systems"

######################################################################
# Set up routes

def setup_routes(app,ui=False):
    app.route("/api/"+name, method="GET", callback=get)
    return


def get():
    try:
        c = bottle.request.route.app.config
        f = bottle.request.query
        args = ["TDWeb",
                name,
                f.get("near"),
                "--detail",
                "--detail",]

        tmp = f.get("lySearchRadius", type=float)
        if tmp != None:
            args.append("--ly")
            args.append(str(tmp))

        return tdweb_run.command(args=args,
                                 tdenv=c.get("tdenv"),
                                 tdb=c.get("tdb"),
                                 render=render)
    except TradeException as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({"error" : str(e)})


def render(results, cmdenv, tdb):
    if not results or not results.rows:
        raise TradeException("No systems found within {}ly of {}.".format(
            results.summary.ly,
            results.summary.near.name(),))

    out = {
        "summary": {
            "near": TDWeb.system_to_dict(cmdenv.nearSystem),
            "lySearchRadius": cmdenv.maxLyPer,
            "resultLimit": cmdenv.limit,
        }, "header": [
            {"type": "system", "name": "System", "path":"system"},
            {"type": "distly", "name": "Distance (LY)", "path": "distLy"},
        ]
    }

    out.update({"rows": [ ]})
    for row in results.rows:
        sys = {"system": TDWeb.system_to_dict(row.system),
               "dist": row.dist,}
        out["rows"].append(sys)

    return TDWeb.pretty_json_dump(out)