import bottle
from tradeexcept import TradeException
from web_commands import tdweb_run
from tradedb import AmbiguityError
from tdweb import TDWeb

######################################################################
# Header
name = "nav"
longname = "Navigation"

######################################################################
# Set up routes

def setup_routes(app,ui=False):
    app.route("/api/"+name, method="GET", callback=get)
    return


def get():
    try:
        c = bottle.request.route.app.config
        f = bottle.request.query

        ladenLy = f.get("ladenLy", type=float)
        emptyLy = f.get("emptyLy", type=float)
        useEmpty = f.get("useEmpty")

        if (
            useEmpty != None and
            useEmpty.lower() in ("yes", "true", "t", "1") and
            emptyLy
            ):
            tmp = emptyLy
        else:
            tmp = ladenLy

        args = ["TDWeb",
                name,
                f.get("from"),
                f.get("to"),
                "--ly",
                str(tmp),
                "--stations",
                "--detail",
                "--detail"]

        tmp = f.get("refuelJumps", type=int)
        if tmp != None:
            args.append("--refuel-jumps")
            args.append(str(tmp))

        return tdweb_run.command(args=args,
                                 tdenv=c.get("tdenv"),
                                 tdb=c.get("tdb"),
                                 render=render)
    except TradeException as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({"error": str(e)})
    except LookupError as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump(
            {"error" : "Error: Unrecognized {}: {}".format("place",
            f.get("item"))}
        )
    return 


def render(results, cmdenv, tdb):
    out = {
        "summary": {
            "fromSys": TDWeb.system_to_dict(results.summary.fromSys),
            "toSys": TDWeb.system_to_dict(results.summary.toSys),
            "maxLy": results.summary.maxLy,
        }, "header": [
            {"type": "text", "name": "Action", "path":"action"},
            {"type": "system", "name": "System", "path":"system"},
            {"type": "distly", "name": "Jump (LY)", "path": "jumpLy"},
            {"type": "distly", "name": "Cumulative (LY)", "path": "distLy"},
            {"type": "distly", "name": "Direct (LY)", "path": "dirLy"},
        ]
    }

    out.update({"rows": [ ]})
    for row in results.rows:
        out["rows"].append(
            { "action": row.action,
              "system": TDWeb.system_to_dict(row.system),
              "jumpLy": row.jumpLy,
              "distLy": row.totalLy,
              "dirLy": row.dirLy,
            })

    return TDWeb.pretty_json_dump(out)