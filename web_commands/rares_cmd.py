import bottle
from tradeexcept import TradeException
from web_commands import tdweb_run
from tradedb import TradeDB, AmbiguityError
from tdweb import TDWeb

######################################################################
# Header
name = "rares"
longname = "Locate Rares"

######################################################################
# Set up routes

def setup_routes(app,ui=False):
    app.route("/api/"+name, method="GET", callback=get)
    return


def get():
    try:
        c = bottle.request.route.app.config
        f = bottle.request.query
        args = ["TDWeb",
                name,
                f.get("near"),]

        tmp = f.get("lySearchRadius", type=float)
        if tmp:
            args.append("--ly")
            args.append(str(tmp))

        tmp = f.get("padSize")
        if tmp:
            args.append("--pad-size")
            args.append(str(tmp))

        tmp = f.get("resultLimit", type=int)
        if tmp:
            args.append("--limit")
            args.append(str(tmp))

        tmp = f.get("sort")
        if tmp == "sortByPrice":
            args.append("--price-sort")

        tmp = f.get("reverse")
        if tmp != None and tmp.lower() in ("yes", "true", "t", "1"):
            args.append("--reverse")

        return tdweb_run.command(args=args,
                                 tdenv=c.get("tdenv"),
                                 tdb=c.get("tdb"),
                                 render=render)

    except TradeException as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({"error" : str(e)})
    return 


def render(results, cmdenv, tdb):
    if not results or not results.rows:
        raise TradeException("No rares found within {}ly of {}.".format(
            results.summary.ly,
            results.summary.near.name(),))

    out = {
        "summary": {
            "near": TDWeb.system_to_dict(cmdenv.nearSystem),
            "lySearchRadius": cmdenv.maxLyPer,
            "resultLimit": cmdenv.limit,
        }, "header": [
          {"type": "rare", "name": "Rare", "path": "item"},
          {"type": "station", "name": "Station", "path": "item.station"},
          {"type": "cr", "name": "Cost", "path": "item.cost"},
          {"type": "tons", "name": "Max Allocation", "path": "item.maxAlloc"},
        ]
    }

    if cmdenv.sortByPrice:
        out["summary"].update({"sort": "Price"})
    else:
        out["summary"].update({"sort": "Ly"})

    if cmdenv.reverse:
        out["summary"].update({"reverse": True})
    else:
        out["summary"].update({"reverse": False})

    out.update({"rows": [ ]})
    for row in results.rows:
        out["rows"].append({"item": TDWeb.item_to_dict(row.rare, tdb)})

    return TDWeb.pretty_json_dump(out)