import bottle
from tradeexcept import TradeException
from web_commands import tdweb_run
from tradedb import TradeDB, Item, RareItem, AmbiguityError
from tdweb import TDWeb

######################################################################
# Header
name = "run"
longname = "Run"

######################################################################
# Set up routes


def setup_routes(app,ui=False):
    app.route("/api/"+name, method="GET", callback=get)
    return

def get():
    try:
        c = bottle.request.route.app.config
        f = bottle.request.query
        args = ["TDWeb",
                name,
                "--detail",
                "--detail",
                "--ly-per",
                str(f.get("ladenLy", type=float)),
                "--credits",
                str(f.get("credits", type=int)),
                "--capacity",
                str(f.get("capacity", type=int))]

        tmp = f.get("from")
        if tmp != None:
            args.append("--from")
            args.append(str(tmp))

        tmp = f.get("to")
        if tmp:
            args.append("--to")
            args.append(str(tmp))

        tmp = f.get("towards")
        if tmp:
            args.append("--towards")
            args.append(str(tmp))

        tmp = f.get("via")
        if tmp:
            args.append("--via")
            args.append(str(tmp))

        tmp = f.get("avoid")
        if tmp:
            args.append("--avoid")
            args.append(str(tmp))

        tmp = f.get("hops",type=int)
        if tmp != None:
            args.append("--hops")
            args.append(str(tmp))

        tmp = f.get("jumps",type=int)
        if tmp != None:
            args.append("--jumps-per")
            args.append(str(tmp))

        tmp = f.get("emptyLy",type=float)
        if tmp != None:
            args.append("--empty-ly")
            args.append(str(tmp))

        tmp = f.get("startJumps",type=int)
        if tmp != None:
            args.append("--start-jumps")
            args.append(str(tmp))

        tmp = f.get("endJumps",type=int)
        if tmp != None:
            args.append("--end-jumps")
            args.append(str(tmp))

        tmp = f.get("itemLimit", type=int)
        if tmp != None:
            args.append("--limit")
            args.append(str(tmp))

        tmp = f.get("maxAge", type=float)
        if tmp != None:
            args.append("--max-days-old")
            args.append(str(tmp))

        tmp = f.get("padSize")
        if tmp != None:
            args.append("--pad-size")
            args.append(str(tmp))

        tmp = f.get("blackMarket")
        if tmp != None and tmp.lower() in ("yes", "true", "t", "1"):
            args.append("--black-market")

        tmp = f.get("lsPenalty", type=float)
        if tmp != None:
            args.append("--ls-penalty")
            args.append(str(tmp))

        tmp = f.get("lsMax", type=int)
        if tmp != None:
            args.append("--ls-max")
            args.append(str(tmp))

        tmp = f.get("unique")
        if tmp != None and tmp.lower() in ("yes", "true", "t", "1"):
            args.append("--unique")

        tmp = f.get("margin", type=float)
        if tmp != None:
            args.append("--margin")
            args.append(str(tmp))

        tmp = f.get("insurance", type=int)
        if tmp != None:
            args.append("--insurance")
            args.append(str(tmp))

        tmp = f.get("routes", type=int)
        if tmp != None:
            args.append("--routes")
            args.append(str(tmp))

        tmp = f.get("maxRoutes", type=int)
        if tmp != None:
            args.append("--max-routes")
            args.append(str(tmp))

        tmp = f.get("pruneScore", type=float)
        if tmp != None:
            args.append("--prune-score")
            args.append(str(tmp))

        tmp = f.get("pruneHops", type=int)
        if tmp != None:
            args.append("--prune-hops")
            args.append(str(tmp))
            
        print(repr(args))

        return tdweb_run.command(args=args,
                                 tdenv=c.get("tdenv"),
                                 tdb=c.get("tdb"),
                                 render=render)
    except AmbiguityError as e:
        bottle.response.status = 303
        return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
    except TradeException as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({"error" : str(e)})
    except LookupError as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({
            "error": "Error: Unrecognized {}: {}".format(
            "item", f.get("item"))
        })


def render(results, cmdenv, tdb):
    routes = results.data
    n = min(len(routes), cmdenv.routes)

    out = {
        "summary": {
            "credits": cmdenv.credits,
            "capacity": cmdenv.capacity,
            "maxLy": cmdenv.maxLyPer,
            "routes": n,
        },
        "routes": [ ],
    }

    if results.summary.exception:
        out["summary"].update(error=str(results.summary.exception))

    # A horrible, horrible, lazy, hack.
    for i in range(0, n):
        r = {
            "stations": [ ],
            "hops": [ ],
            "gainCr": routes[i].gainCr,
            "score": routes[i].score,
        }
        for stn in routes[i].route:
            r["stations"].append(TDWeb.station_to_dict(stn,True))
        for j, hop in enumerate(routes[i].hops):
            r["hops"].append({
                "trades": [ {
                    "item": TDWeb.item_to_dict(trade.item, tdb),
                    "quantity": qty,
                    "gainCr": trade.gainCr,
                    "costCr": trade.costCr,
                    "stock": trade.stock,
                    "stockLevel": trade.stockLevel,
                    "demand": trade.demand,
                    "demandLevel": trade.demandLevel,
                    "srcAge": trade.srcAge,
                    "dstAge": trade.dstAge,
                } for (trade, qty) in hop[0] ],
                "buyAt": routes[i].route[j].name(),
                "sellAt": routes[i].route[j+1].name(),
                "gainCr": hop.gainCr,
                "units": hop.units,
                "jumps": [ TDWeb.system_to_dict(x,True)
                    for x in routes[i].jumps[j]
                ],
            })
        out["routes"].append(r);

    return TDWeb.pretty_json_dump(out)
