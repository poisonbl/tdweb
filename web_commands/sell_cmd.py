import bottle
from tradeexcept import TradeException
from web_commands import tdweb_run
from tradedb import TradeDB, Item, RareItem, AmbiguityError
from tdweb import TDWeb

######################################################################
# Header
name = "sell"
longname = "Sell"

######################################################################
# Set up routes

def setup_routes(app,ui=False):
    app.route("/api/"+name,method="GET",callback=get)
    return


def get():
    try:
        c = bottle.request.route.app.config
        f = bottle.request.query
        args = ["TDWeb",
                name,
                "--detail",
                "--detail",
                f.get("item"),]

        tmp = f.get("resultLimit", type=int)
        if tmp != None:
            args.append("--limit")
            args.append(str(tmp))

        tmp = f.get("near")
        if tmp != None:
            args.append("--near")
            args.append(str(tmp))
            tmp = f.get("lySearchRadius",type=float)
            if tmp != None:
                args.append("--ly")
                args.append(str(tmp))

        tmp = f.get("padSize")
        if tmp != None:
            args.append("--pad-size")
            args.append(str(tmp))

        tmp = f.get("sort")
        if tmp == "sortByPrice":
            args.append("--price-sort")

        tmp = f.get("ltCr", type=int)
        if tmp != None:
            args.append("--lt")
            args.append(str(tmp))

        tmp = f.get("gtCr", type=int)
        if tmp != None:
            args.append("--gt")
            args.append(str(tmp))

        return tdweb_run.command(args=args,
                                 tdenv=c.get("tdenv"),
                                 tdb=c.get("tdb"),
                                 render=render)
    except AmbiguityError as e:
        bottle.response.status = 303
        return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
    except TradeException as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({"error" : str(e)})
    except LookupError as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump(
            {"error" : "Error: Unrecognized {}: {}".format("item",
            f.get("item"))})


def render(results, cmdenv, tdb):

    out = {
        "summary": {
            "sort": results.summary.sort,
            "resultLimit": cmdenv.limit,
            "item": TDWeb.item_to_dict(results.summary.item, tdb)
        }, "header": [
          {"type": "station", "name": "Station", "path":"station"},
          {"type": "cr", "name": "Cost", "path": "cost"},
          {"type": "tons", "name": "Demand", "path": "demand"},
          {"type": "age", "name": "Age", "path": "age"},
        ]
    }

    if cmdenv.nearSystem:
        out["summary"].update({"near": TDWeb.system_to_dict(results.summary.near),
                               "lySearchRadius": results.summary.ly})
        out["header"].append({"type": "distly", "name": "Distance (LY)",
                            "path": "distLy"})

    out.update({"rows": [ ]})
    for row in results.rows:
        r = {
            "station": TDWeb.station_to_dict(row.station, True),
            "cost": row.price,
            "age": row.age,
            "demand": row.demand,
        }
        if cmdenv.nearSystem:
            r.update({"distLy": row.dist})

        out["rows"].append(r)

    return TDWeb.pretty_json_dump(out)