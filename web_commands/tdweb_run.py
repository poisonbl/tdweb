from commands import CommandIndex

def command(args,tdenv,tdb,render):
    cmdIndex = CommandIndex()
    cmdenv = cmdIndex.parse(args)

    # Since we have no way to step through the output...
    cmdenv.x52pro = False
    cmdenv.mfd = False

    if cmdenv.usesTradeData:
        tsc = tdb.tradingStationCount
        if tsc == 0:
            raise exceptions.NoDataError(
                "There is no trading data for ANY station in "
                "the local database. Please enter or import "
                "price data."
            )
        if tsc == 1:
            raise exceptions.NoDataError(
                "The local database only contains trading data "
                "for one station. Please enter or import data "
                "for additional stations."
            )
        if tsc < 8:
            cmdenv.NOTE(
                "The local database only contains trading data "
                "for {} stations. Please enter or import data "
                "for additional stations.".format(
                tsc
            ))

    results = cmdenv.run(tdb)
    return render(results,cmdenv,tdb)