import bottle
from tradeexcept import TradeException
from web_commands import tdweb_run
from tradedb import TradeDB, Item, RareItem, AmbiguityError
from tdweb import TDWeb

######################################################################
# Header
name = "trade"
longname = "Trade"

######################################################################
# Set up routes

def setup_routes(app,ui=False):
    app.route("/api/"+name,method="GET",callback=get)
    return


def get():
    try:
        c = bottle.request.route.app.config
        f = bottle.request.query
        args = ["TDWeb",
                name,
                "--detail",
                "--detail",
                f.get("fromStn"),
                f.get("toStn"),]

        return tdweb_run.command(args=args,
                                 tdenv=c.get("tdenv"),
                                 tdb=c.get("tdb"),
                                 render=render)
    except AmbiguityError as e:
        bottle.response.status = 303
        return TDWeb.pretty_json_dump(TDWeb.ambiguity_error_to_dict(e))
    except TradeException as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump({"error" : str(e)})
    except LookupError as e:
        bottle.response.status = 400
        return TDWeb.pretty_json_dump(
            {"error" : "Error: Unrecognized {}: {}".format("item",
            f.get("item"))})


def render(results, cmdenv, tdb):

    out = {
        "summary": {
            "fromStn": TDWeb.station_to_dict(results.summary.fromStation),
            "toStn": TDWeb.station_to_dict(results.summary.toStation),
        }, "header": [
          {"type": "item", "name": "Item", "path": "item"},
          {"type": "cr", "name": "Profit", "path": "profit"},
          {"type": "cr", "name": "Cost", "path": "cost"},
          {"type": "tons", "name": "Stock", "path": "stock"},
          {"type": "tons", "name": "Demand", "path": "demand"},
          {"type": "age", "name": "Source Age", "path": "srcAge"},
          {"type": "age", "name": "Destination Age", "path": "dstAge"},
        ]
    }

    out.update({"rows":[ ]})
    for row in results.rows:
        r = {
            "item": TDWeb.item_to_dict(row.item, tdb),
            "profit": row.gainCr,
            "cost": row.costCr,
            "stock": row.stock,
            "demand": row.demand,
            "srcAge": row.srcAge,
            "dstAge": row.dstAge,
        }
        out["rows"].append(r)

    return TDWeb.pretty_json_dump(out)